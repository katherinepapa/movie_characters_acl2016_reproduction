package thesis.Parser;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import thesis.characters.database.AnnotationJDBC;
import thesis.characters.model.Annotation;
import thesis.characters.model.Credit;
import thesis.characters.model.Item;
import thesis.characters.utilities.ExtractCharacterCreditIndex;
import thesis.characters.utilities.ExtractMovieCategory;

/**
 * Class ESParser parses annotations
 *
 * @author katerina papantoniou
 */
public class ESParser {

    public static void main(String args[]) throws UnsupportedEncodingException, IOException, InterruptedException {
        AnnotationJDBC ann_obj = new AnnotationJDBC();
        ExtractMovieCategory ext_obj = new ExtractMovieCategory();
        ESParser a_obj = new ESParser();
        String file = "files/annotation.json";

//read the file
        InputStream in = a_obj.getClass().getClassLoader().getResourceAsStream("" + file);
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

        java.lang.reflect.Type listType = new TypeToken<ArrayList<Item>>() {
        }.getType();
        List<Item> yourClassList = new Gson().fromJson(reader, listType);

        for (Item yourClassList1 : yourClassList) {
            System.out.println("------------------------------------------------");

            if (yourClassList1.getSource().getTarget().size() >= 1) {
                //get text

                if (yourClassList1.getSource().getText() != null) {
                    if (yourClassList1.getSource().getText().equalsIgnoreCase("positive") || yourClassList1.getSource().getText().startsWith("n") || yourClassList1.getSource().getText().startsWith("p") || yourClassList1.getSource().getText().equalsIgnoreCase("negative") || yourClassList1.getSource().getText().equalsIgnoreCase("neutral")) {
                        // yourClassList.get(i).getSource().
                        if (yourClassList1.getSource().getTarget().get(0).getSelector().size() >= 3) {
                            if (yourClassList1.getSource().getTarget().get(0).getSelector().get(2).getType().equals("TextQuoteSelector")) {
                                ArrayList<Credit> credit_list;
                                ExtractCharacterCreditIndex t = new ExtractCharacterCreditIndex();
                                credit_list = t.extractCreditList(yourClassList1.getSource().getDocument().getLink().get(1).getHref(), yourClassList1.getSource().getDocument().getTitle());

                                String title = yourClassList1.getSource().getDocument().getTitle();
                                String year = "";

                                Pattern p = Pattern.compile("(\\d+)");
                                Matcher m = p.matcher(title);
                                while (m.find()) {
                                    year = m.group(0);
                                    String title_only = title.replace(year, "");
                                }

                                Annotation obj = new Annotation();
                                obj.setAnnotator_name(yourClassList1.getSource().getUser());
                                obj.setCharacter_name(yourClassList1.getSource().getTarget().get(0).getSelector().get(2).getExact().replace("'", ""));
                                obj.setImdb_url(yourClassList1.getSource().getDocument().getLink().get(1).getHref());
                                obj.setTag(yourClassList1.getSource().getText());
                                obj.setYear(year);
                                obj.setMovie_title(title);

                                for (Credit credit_list1 : credit_list) {
                                    String chara = credit_list1.getCharacter_name();
                                    chara = chara.replaceAll("\"", "");
                                    chara = chara.replace("\'", "");
                                    chara = chara.replace("ó", "o");

                                    if (obj.getCharacter_name().contains(chara) || chara.contains(obj.getCharacter_name())) {
                                        obj.setCredit_index(credit_list1.getCredit_index());
                                        break;
                                    }
                                }

                                ArrayList<String> category_list = ext_obj.extractCategory(obj.getImdb_url().replace("/fullcredits", ""));
                                obj.setMovie_genre(category_list.get(0));

                                ann_obj.insert(obj);
                            }
                        }
                    }
                }
            }
            System.out.println("------------------------------------------------");
        }

    }

}
