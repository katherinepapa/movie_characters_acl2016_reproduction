package thesis.characters.features;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 * This class removes features from the arff
 * https://weka.wikispaces.com/Remove+Attributes
 *
 * @author katherine papantoniou
 */
public class ArrfCreationSelected {

    /**
     * takes an ARFF file as first argument, the number of indices to remove as
     * second and thirdly whether to invert or not (true/false). Dumps the
     * generated data to stdout.
     */
    public static void main(String[] args) throws Exception {
        Instances inst;
        ArrfCreationSelected arff = new ArrfCreationSelected();
        inst = new Instances(new BufferedReader(new FileReader("data.arff")));

        arff.withoutDomain(inst);
        arff.onlyDomain(inst);
        arff.withoutEmotions(inst);

        arff.phonologicalAttributes(inst, true, "data_only_phonological_features");
        arff.phonologicalAttributes(inst, false, "data_without_phonological_features");
        arff.withoutPoetic(inst, false, "data_without_poetic_features");
        arff.withoutPoetic(inst, true, "data_without_consonance_features");
        arff.withoutSocial(inst);

    }

    public void withoutDomain(Instances inst) throws Exception {
        Instances instNew;
        Remove remove;
        remove = new Remove();
        int i[] = new int[3];
        i[0] = 10;
        i[1] = 12;
        i[2] = 14;
        remove.setAttributeIndicesArray(i);
        remove.setInvertSelection(false);
        remove.setInputFormat(inst);
        instNew = Filter.useFilter(inst, remove);
    
        writeData(instNew, "data_without_domain_features.arff");

    }

    public void withoutPoetic(Instances inst, boolean withoutConsonance, String fileName) throws Exception {
        Instances instNew;
        Remove remove;
        remove = new Remove();
        int i[];
        if (withoutConsonance == false) {
            i = new int[3];
            i[0] = 4;
            i[1] = 5;
            i[2] = 24;
        } else {
            i = new int[1];
            i[0] = 4;
        
        }
        remove.setAttributeIndicesArray(i);
        remove.setInvertSelection(false);
        remove.setInputFormat(inst);
        instNew = Filter.useFilter(inst, remove);

        writeData(instNew, fileName + ".arff");

    }

    public void withoutSocial(Instances inst) throws Exception {
        Instances instNew;
        Remove remove;
        remove = new Remove();
        int i[] = new int[4];
        i[0] = 17;
        i[1] = 18;
        i[2] = 13;
        i[3] = 24;
        remove.setAttributeIndicesArray(i);
        remove.setInvertSelection(false);
        remove.setInputFormat(inst);
        instNew = Filter.useFilter(inst, remove);

        writeData(instNew, "data_without_social_features.arff");

    }

    public void withoutEmotions(Instances inst) throws Exception {
        Instances instNew;
        Remove remove;
        remove = new Remove();
        int i[] = new int[2];
        i[0] = 15;
        i[1] = 16;
        remove.setAttributeIndicesArray(i);
        remove.setInvertSelection(new Boolean(false).booleanValue());
        remove.setInputFormat(inst);
        instNew = Filter.useFilter(inst, remove);
       
        writeData(instNew, "data_without_emotion_features.arff");

    }

    public void onlyDomain(Instances inst) throws Exception {
        Instances instNew;
        Remove remove;
        remove = new Remove();
        int i[] = new int[4];
        i[0] = 10;
        i[1] = 12;
        i[2] = 14;
        i[3] = 28;
        remove.setAttributeIndicesArray(i);
        remove.setInvertSelection(true);
        remove.setInputFormat(inst);
        instNew = Filter.useFilter(inst, remove);
        
        writeData(instNew, "data_only_domain_features.arff");

    }

    public void phonologicalAttributes(Instances inst, boolean invert, String fileName) throws Exception {
        Instances instNew;
        Remove remove;
        remove = new Remove();
        int i[] = null;
        if (invert == true) {
            i = new int[14];
            i[0] = 0;
            i[1] = 1;
            i[2] = 2;
            i[3] = 3;
            i[4] = 6;
            i[5] = 7;
            i[6] = 8;
            i[7] = 9;
            i[8] = 19;
            i[9] = 20;
            i[10] = 21;
            i[11] = 22;
            i[12] = 23;
            i[13] = 28;
        } else {
            i = new int[13];
            i[0] = 0;
            i[1] = 1;
            i[2] = 2;
            i[3] = 3;
            i[4] = 6;
            i[5] = 7;
            i[6] = 8;
            i[7] = 9;
            i[8] = 19;
            i[9] = 20;
            i[10] = 21;
            i[11] = 22;
            i[12] = 23;
         
        }

        remove.setAttributeIndicesArray(i);
        remove.setInvertSelection(invert);
        remove.setInputFormat(inst);
        instNew = Filter.useFilter(inst, remove);
       
        writeData(instNew, fileName + ".arff");

    }

    public static String printHeader(String decription) {
        String s;
        Date date = new Date();

        // display time and date using toString()
        s = "% 1. Title: Movies Arff file " + decription + "\n"
                + "%" + "\n"
                + "% 2. Sources:" + "\n"
                + "%      (a) Creator: Katherina Papantoniou" + "\n"
                + "%      (c) Date: " + date.toString() + "\n"
                + " %\n";

        return s;

    }

    public void writeData(Instances instNew, String fileName) throws IOException, InterruptedException {

        try (FileWriter f = new FileWriter(fileName)) {
            f.write(printHeader(fileName.split("\\.")[0]));
            f.write(instNew.toString());
            f.close();
        } catch (IOException ex) {
            Logger.getLogger(ArrfCreationSelected.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
