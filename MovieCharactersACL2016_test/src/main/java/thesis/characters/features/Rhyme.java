package thesis.characters.features;

import java.util.StringTokenizer;

public class Rhyme {

    public static String getLastWord(String line) {

        StringTokenizer parser = new StringTokenizer(line);

        String lastWord = "";

        while (parser.hasMoreTokens() == true) {
            lastWord = parser.nextToken();
        }

        return lastWord;

    }

    public static boolean endRhymePresent(String word1, String word2) {

        for (int i = 1; i <= word1.length(); i++) { //iterate thru shorter string (caller must pass shorter word first)

            int stringPosition1 = word1.length() - i;       // start at end of word and iterate leftwards
            int stringPosition2 = word2.length() - i;

            String substring1 = word1.substring(stringPosition1);
            String substring2 = word2.substring(stringPosition2);

            if (substring1.equalsIgnoreCase(substring2)) {  // substrings match (ie. matching termination)

                if (Phoneme.isVowel(word1.charAt(stringPosition1))) {  // current phoneme is vowel

                    if (stringPosition1 - 1 <= 0) { // no more phonemes!

                        return true;

                    }

                    if (Phoneme.isVowel(word1.charAt(stringPosition1 - 1))) { // next phoneme is vowel

                    } else {  // found a consonant followed by vowel sound & matching termination

                        if (word1.charAt(stringPosition1) == word2.charAt(stringPosition2)) {  // same consonants - no rhyme unless next is consonant and different

                            if ((word1.charAt(stringPosition1 - 2) != word2.charAt(stringPosition2 - 2))) { // next are different

                                if (Phoneme.isVowel(word1.charAt(stringPosition1 - 2)) == false & Phoneme.isVowel(word2.charAt(stringPosition2 - 2)) == false) { // next are consonants

                                    return true;

                                }
                            }

                            if ((stringPosition1 - 1) == 0) { // no more phonemes!

                                return false;
                            } else {
                                continue; // do next iteration
                            }
                        } else { // it's a rhyme! wow!

                            return true;

                        }

                    }

                } else {  // not a vowel - increase substring

                    continue; // do next iteration

                }

            } else {  // no match in termination - no rhyme

                return false;

            }

        }

        return false;
    }

    public static boolean repetitionPresent(String line1, String line2) {

        return false;

    }

    public static boolean internalRhymePresent(String line1) {

        return false;

    }

}
