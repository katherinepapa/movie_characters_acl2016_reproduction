package thesis.characters.features;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import thesis.characters.database.AnnotationJDBC;
import thesis.characters.model.Annotation;
import thesis.characters.model.Features;
import static thesis.characters.utilities.Utilities.removeSpecialCharacters;

/**
 * Generates a little ARFF file with different attribute types.
 *
 * @author fileas
 */
public class ArffCreation {

    private static final Logger log = Logger.getLogger(ArffCreation.class.getName());

    public static void main(String[] args) throws Exception {

        ArffCreation a = new ArffCreation();

        FileWriter f = new FileWriter("data.arff");
        BufferedWriter b = new BufferedWriter(f);
        String text = "";
        text = a.printHeader();
        text += a.createRelation();
        text += a.createAttributes();

        text += a.createData();

        b.write(text);
        b.newLine();
        b.close();

    }

    public String printHeader() {
        String s;
        Date date = new Date();

        // display time and date using toString()
        s = "% 1. Title: Movies Arff file" + "\n"
                + "%" + "\n"
                + "% 2. Sources:" + "\n"
                + "%      (a) Creator: Katherina Papantoniou" + "\n"
                + "%      (c) Date: " + date.toString() + "\n"
                + " %\n";

        return s;

    }

    public String createRelation() {
        String s;
        s = "@RELATION movies\n";
        return s;
    }

    public String createAttributes() throws IOException {
        String s = null;

        //feature_obj.
        //feature_obj.
        //2. NumWords
        //3. NumOfVowels
        //4. Consonance
        //5. Assonance
        //6. NumOfConsonants
        //7. NumberOfPlosives
        //8. NumberOfFricatives
        //9. NumberOfAffricate
        //10. NumberOfNasals
        //11. Rhyme
        s = "@ATTRIBUTE numsyllabes  NUMERIC" + "\n"
                + "@ATTRIBUTE numwords  NUMERIC" + "\n"
                + "@ATTRIBUTE numvowels  NUMERIC" + "\n"
                + "@ATTRIBUTE numconsonants  NUMERIC" + "\n"
                + "@ATTRIBUTE consonance  NUMERIC" + "\n"
                + "@ATTRIBUTE assonance  NUMERIC" + "\n"
                + "@ATTRIBUTE numplosives  NUMERIC" + "\n"
                + "@ATTRIBUTE numfricatives  NUMERIC" + "\n"
                + "@ATTRIBUTE numaffricate  NUMERIC" + "\n"
                + "@ATTRIBUTE numnasal  NUMERIC" + "\n"
                + "@ATTRIBUTE sameastitle  NUMERIC" + "\n"
                + "@ATTRIBUTE article  NUMERIC" + "\n"
                + "@ATTRIBUTE genre  {'Adventure','Western','Action','Musical', 'Mystery','Animation','Sci-Fi','Biography','Thriller','Crime', 'Drama','Comedy','Horror'}" + "\n"
                + "@ATTRIBUTE honor  NUMERIC" + "\n"
                + "@ATTRIBUTE credit_index  NUMERIC" + "\n"
                + "@ATTRIBUTE sentimentsoundexwordnet  NUMERIC" + "\n"
                + "@ATTRIBUTE sentimentlevenshteinwordnet  NUMERIC" + "\n"
                + "@ATTRIBUTE foreignsuffix  NUMERIC" + "\n"
                + "@ATTRIBUTE firstnamefreq  NUMERIC" + "\n"
                + "@ATTRIBUTE startcv  {'consonant','vowel'}" + "\n"
                + "@ATTRIBUTE endcv  {'consonant','vowel'}" + "\n"
                + "@ATTRIBUTE voice  {'other','voiced','voiceless'}" + "\n"
                + "@ATTRIBUTE  numberOfSubsequentPhonems  NUMERIC" + "\n"
                + "@ATTRIBUTE  sameuppercase  NUMERIC" + "\n"
                + "@ATTRIBUTE gender  {'F','M','N'}" + "\n"
                + "@ATTRIBUTE alliteration  NUMERIC" + "\n"
                + "@ATTRIBUTE high   NUMERIC" + "\n"
                + "@ATTRIBUTE low   NUMERIC" + "\n"
                + "@ATTRIBUTE class {positive,negative}" + "\n" + "\n";

        return s;
    }

    public ArrayList<String> handleSplitterCharacter(String character) {
        ArrayList<String> c_list = new ArrayList<>();
        String c[] = character.split("/|\\(");

        for (String c1 : c) {
            c_list.add(c1.trim().replace(")", ""));
        }

        return c_list;
    }

    public String createData() throws IOException {

        FileWriter f = new FileWriter("log_file.txt");
        BufferedWriter b = new BufferedWriter(f);

        String s;

        s = "@DATA " + "\n";

        FeaturesExtraction feature_obj = new FeaturesExtraction();

        //get names
        AnnotationJDBC j_obj = new AnnotationJDBC();
        ArrayList<String> character_list;
        character_list = j_obj.getCharacterNames();
        String phonem = "";

        char[] phonems_list;

        for (String character_list1 : character_list) {
            String character_name = "";
            ArrayList<String> c_list = handleSplitterCharacter(character_list1);// an einai dio onomata se mia eggrafi p.x Asdsd Fsdsd as Fred
            for (String c_list1 : c_list) {
                character_name = character_name + " " + c_list1;
                character_name = character_name.trim();
                phonems_list = character_name.toUpperCase().toCharArray();
                // feature_modobj.feature_obj.getNumWords(phonem);
                //extract attributes
                //attributes names
                //1. NumSyllables
                Features feature_modobj = new Features();
                int numsyllabes = feature_obj.getNumSyllables(character_name);
                feature_modobj.setNumsyllabes(numsyllabes);
                feature_modobj.setNumwords(feature_obj.getNumWords(character_name));
                feature_modobj.setNumvowels(feature_obj.numOfVowels(character_name));
                feature_modobj.setNumconsonants(feature_obj.numOfConsonants(character_name));

                feature_modobj.setFreqassonance(feature_obj.calculateAssonance(phonems_list));
                feature_modobj.setFreqconsonance(feature_obj.calculateConsonance(phonems_list));

                feature_modobj.setNumplosives(feature_obj.numberOfPlosives(character_name));
                feature_modobj.setNumfricatives(feature_obj.numberOfFricatives(character_name));

                feature_modobj.setNumaffricate(feature_obj.numberOfAffricate(character_name));
                feature_modobj.setNumnasals(feature_obj.numberOfNasals(character_name));
                feature_modobj.setForeignsuffix(feature_obj.getForeignFeature(character_name));
                feature_modobj.setStartcv(feature_obj.startCV(character_name.trim()));
                // System.out.println("Start CV: " + feature_obj.startCV(character_name.trim()));
                feature_modobj.setEndcv(feature_obj.endCV(character_name));
                feature_modobj.setVoice(feature_obj.startVoice(character_name.trim().toLowerCase()));
                feature_modobj.setNumsubsequent(feature_obj.numberOfSubsequentPhonems(character_name.trim().toLowerCase()));
                feature_modobj.setSameuppercase(feature_obj.sameUpperCase(character_name));
                feature_modobj.setGender(feature_obj.getGender(character_name));
                feature_modobj.setAlliteration(feature_obj.calculateAlliteration(character_name));

                int[] k = feature_obj.calculateHighLow(character_name);
                feature_modobj.setHigh(k[0]);
                feature_modobj.setLow(k[1]);

//***************************************/
                character_name = removeSpecialCharacters(character_name);
                character_name = character_name.trim();
                String sw[] = character_name.split(" ");

                ArrayList<String> name_words = new ArrayList<>(Arrays.asList(sw));

                for (String sw1 : sw) {
                    double first_freq = feature_obj.getFrequencyName(sw1, "frequencies_first_name.csv");
                    if (first_freq > 0.0) {
                        feature_modobj.setFirst_name_freq(first_freq);
                        break;
                    }
                }

                feature_modobj.setHonourTitle(feature_obj.hasHounourTitle(name_words));

                //be careful
                ArrayList<Annotation> ann_obj = j_obj.getAnnotation(character_list1);
                feature_modobj.setCreditIndex(ann_obj.get(0).getCredit_index());

                feature_modobj.setSentimentconnection(feature_obj.getSentimentConnection(name_words));

                feature_modobj.setSentimentLevenshteinconnection(feature_obj.getSentimentLeveinsteinConnection(name_words));

//***************************************/
                ArrayList<Annotation> annotation_tag;
                log.log(Level.INFO, "character_name: " + character_list1, character_list1);

                String tag = "";
                String title = "";
                String genre = "";
                String total_tag = "";
                for (Annotation annotation_tag1 : ann_obj) {
                    tag = annotation_tag1.getTag();
                    title = annotation_tag1.getMovie_title().toLowerCase();

                    genre = annotation_tag1.getMovie_genre();

                    genre = genre.replace("Certificate:", "");
                    genre = genre.replace("K-13", "");
                    genre = genre.replace("K-17", "");
                    genre = genre.replace("K", "");
                    genre = genre.replace("13", "");
                    genre = genre.replace("17", "");
                    genre = genre.trim();

                    if ((tag.equals("positive") || (tag.startsWith("neg")) || (tag.equals("negative"))) && !tag.equals("neutral")) {
                        log.log(Level.INFO, "tag: " + tag, tag);
                        total_tag = total_tag + tag;

                    } else {

                    }
                }

                if (total_tag.contains("neg")) {
                    tag = "negative";
                } else {
                    tag = "positive";
                }

                feature_modobj.setGenre(genre);

                if (title.contains(character_list1.toLowerCase().trim())) {
                    feature_modobj.setSameastitle(1);

                } else {
                    feature_modobj.setSameastitle(0);
                }

                if (c_list1.contains("The") || c_list.contains("the ")) {
                    feature_modobj.setArticle(1);

                } else {
                    feature_modobj.setArticle(0);
                }

                if (!tag.equals("")) {
                    s = s + feature_modobj.getNumsyllabes() + "," + feature_modobj.getNumwords() + ","
                            + feature_modobj.getNumvowels() + ","
                            + feature_modobj.getNumconsonants() + ","
                            + feature_modobj.getFreqconsonance() + ","
                            + feature_modobj.getFreqassonance() + ","
                            + feature_modobj.getNumplosives() + ","
                            + feature_modobj.getNumfricatives() + ","
                            + feature_modobj.getNumaffricate() + ","
                            + feature_modobj.getNumnasals() + ","
                            + feature_modobj.getSameastitle() + ","
                            + feature_modobj.getArticle() + ","
                            + feature_modobj.getGenre() + ","
                            + feature_modobj.getHonourTitle() + ","
                            + feature_modobj.getCreditIndex() + ","
                            + feature_modobj.getSentimentconnection() + ","
                            + feature_modobj.getSentimentLevenshteinconnection() + ","
                            + feature_modobj.getForeignsuffix() + ","
                            + feature_modobj.getFirst_name_freq() + ","
                            + feature_modobj.getStartcv() + ","
                            + feature_modobj.getEndcv() + ","
                            + feature_modobj.getVoice() + ","
                            + feature_modobj.getNumsubsequent() + ","
                            + feature_modobj.getSameuppercase() + ","
                            + feature_modobj.getGender() + ","
                            + feature_modobj.getAlliteration() + ","
                            + feature_modobj.getHigh() + ","
                            + feature_modobj.getLow() + ","
                            + tag + "\n";
                }
            }
        }

        //feature_obj.
        //feature_obj.
        //2. NumWords
        //3. NumOfVowels
        //4. Consonance
        //5. Assonance
        //6. NumOfConsonants
        //7. NumberOfPlosives
        //8. NumberOfFricatives
        //9. NumberOfAffricate
        //10. NumberOfNasals
        //11. Rhyme
        return s;
    }
}
