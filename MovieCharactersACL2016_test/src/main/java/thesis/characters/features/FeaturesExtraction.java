/**
 * Texnoglossia Thesis Class for the extraction of features
 *
 */
package thesis.characters.features;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import thesis.characters.database.SentiWordnetJDBC;
import thesis.characters.model.NameFrequency;
import thesis.characters.phonemes.Espeak;
import thesis.characters.phonemes.TextToPhonemes;
import static thesis.characters.phonemes.TextToPhonemes.toPhoneme;

import static thesis.characters.utilities.Utilities.removeSpecialCharacters;

/**
 * Features List length of a name is a common name is a proper noun if it is a
 * common noun belongs to a specific category (historic, animal) it is a
 * sentiment word or the phonem is a sentiment word is a vowel has last and
 * surname rhythm foreign name Many codes for the categories of phonems are
 * taken from here
 * http://www.tuninst.net/BG-MLC/BG-MLC1-2/prop-con3/prop-con3.htm#affricate-wiki
 *
 * @author katerina papantoniou
 */
public class FeaturesExtraction {

  

    /**
     * Same Upper Case Letters
     *
     * @param character_name
     * @return
     */
    public int sameUpperCase(String character_name) {
        int value = 0;

        String s[] = character_name.split(" ");
        for (String first1 : s) {
            for (int j = 1; j < s.length - 1; j++) {
                String first2 = s[j];
                if (first1.equals(first2)) {
                    value = 1;

                }
            }
        }

        return value;
    }

    /**
     * Returns the average number of syllables
     *
     * @param name
     * @return
     */
    public int getNumSyllables(String name) {

        String phonemes = TextToPhonemes.toPhoneme(name);
        int numVowels = 0;
        String s[] = name.split(" ");

        for (int i = 0; i < phonemes.length(); i++) {
            if (TextToPhonemes.isVowel(phonemes.charAt(i))) {
                numVowels++;
                // check if it's a diphthong - if so skip this as next phoneme will inc count anyway
                if (TextToPhonemes.isDiphthong(phonemes.charAt(i), phonemes.charAt(i + 1))) {
                    i++;
                }
            }
        }
        return numVowels / s.length;
    }

    /**
     * Return number of words
     *
     * @param name
     * @return
     */
    public int getNumWords(String name) {
        int num = 0;
        name = name.trim();
        String s[] = name.split(" ");
        num = s.length;
        return num;

    }

    /**
     *
     * @param name
     * @return
     */
    public int calculateLength(String name) {
        int length = 0;
        length = name.length();
        return length;

    }

    /**
     * Get frequency name
     *
     * @param name
     * @param filename
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public double getFrequencyName(String name, String filename) throws FileNotFoundException, IOException {
        ArrayList<NameFrequency> list = new ArrayList<>();
        int total_count = 0;
        FileReader f = new FileReader(filename);
        BufferedReader b = new BufferedReader(f);
        String input = "";
        int rank = 0;
        // System.out.println(filename);
        do {
            input = b.readLine();
            rank = rank + 1;
           
            if (input != null) {
              
                if (!input.equals("") && !input.contains("\"")) {
                    NameFrequency obj = new NameFrequency();
                    String split[] = input.split(",");
                   
                    if (Integer.parseInt(split[1]) >= 8) {
                        obj.setName(split[0]);
                        if (Integer.parseInt(split[1]) >= 101069) {
                            obj.setFrequency(1);
                        } else {
                            obj.setFrequency(0);
                        }

                        total_count = total_count + Integer.parseInt(split[1]);
                        list.add(obj);
                    }

                }
            }
        } while (input != null);

        double freq = 0;

        for (NameFrequency first_list1 : list) {
            String s[] = name.split(" ");
            for (String item : s) {
                if (first_list1.getName().equalsIgnoreCase(item)) {
                    freq = (first_list1.getFrequency());
                    
                    return freq;
                }
            }

        }

        return freq;
    }

    /**
     * Retrieve the gender of a character name
     *
     * @param character_name
     * @return
     * @throws java.io.FileNotFoundException
     */
    public String getGender(String character_name)
            throws FileNotFoundException, IOException {
        String gender = "N";
        FileReader f = new FileReader("names_all.txt");
        BufferedReader bf = new BufferedReader(f);
        String input = "";
        int count_male = 0;
        int count_female = 0;
        do {
            input = bf.readLine();
            if (input != null) {
                String c[] = input.split(",");
                if (c.length >= 2) {
                    String s[] = character_name.split(" ");
                    for (String item : s) {
                        if (c[0].trim().equalsIgnoreCase(item.trim())) {
                          //  System.out.println("%%%%" + c[1].trim());
                            switch (c[1].trim()) {
                                case "M":
                                    count_male++;
                                    break;
                                case "F":
                                    count_female++;
                                    break;
                            }
                          //  System.out.println("%%%%" + count_male);
                          //  System.out.println("%%%%" + count_female);

                        }
                    }

                }

            }
        } while (input != null);

        if (count_male > count_female) {
            return "M";
        } else if (count_male < count_female) {
            return "F";
        } else {
            return "N";
        }

    }

    /**
     * Get foreign suffix
     *
     * @param name
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public int getForeignFeature(String name) throws FileNotFoundException, IOException {

        FileReader f = new FileReader("suffixes.csv");
        BufferedReader bf = new BufferedReader(f);
        ArrayList<String> suffix_list = new ArrayList<String>();
        String input = "";
        do {
            input = bf.readLine();
            if (input != null) {
                input = input.toLowerCase();
                input = input.replace("phd ", "");
                input = input.replace("msc ", "");
                input = input.replace(" msc", "");
                input = input.replace(" phd", "");
                input = input.replace(" dr", "");
                input = input.replace("dr ", "");
                input = input.replace(" sen", "");
                input = input.replace("sen ", "");

                input = input.replace(" sir", "");
                input = input.replace("sir ", "");
                input = input.replace(".", "");
                suffix_list.add(input);
            }
        } while (input != null);
        bf.close();

        int value = 0;
        String s[] = name.split(" ");
        if (s.length > 1) {

            ArrayList<String> name_list = new ArrayList<>();
            name_list.add(s[0]);
            int b = hasHounourTitle(name_list);//is Dr, Professor
            if (b == 1) {
                value = 0;
                if (s.length >= 2) {
                    value = 0;
                    for (String suffix_list1 : suffix_list) {
                        if (s[s.length - 1].endsWith(suffix_list1)) {//
                            value = 1;
                            System.out.println(s[s.length - 1]);
                            break;
                        }
                    }

                }
            } else {
                value = 0;
                if (s.length >= 2) {
                    value = 0;
                    for (String suffix_list1 : suffix_list) {
                        if (s[s.length - 1].endsWith(suffix_list1)) {
                            System.out.println(s[s.length - 1]);
                            value = 1;
                            break;
                        }
                    }
                }
            }

        }

        return value;
    } //    public int getForeignSuffix(String name) throws FileNotFoundException, IOException {
    //        //read the file
    //
    //        FileReader f = new FileReader("deathLM.csv");
    //        BufferedReader bf = new BufferedReader(f);
    //        String input = "";
    //        do {
    //            input = bf.readLine();
    //            if (input != null) {
    //                if (input.endsWith("</s>")) {
    //                    
    //
    //                }
    //
    //            }
    //        } while (input != null);
    //
    //        int fsuffix = 0;
    //        String last4chars = name.substring(name.length(), name.length() - 4);
    //
    //        return fsuffix;
    //    }

    /**
     * Number of vowels in a given string
     *
     * @param name
     * @return
     */
    public int numOfVowels(String name) {
        int vowels = 0;
        char c[] = name.toLowerCase().toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == 'a' || c[i] == 'e' || c[i] == 'i'
                    || c[i] == 'o' || c[i] == 'u') {
                vowels++;
            }

        }

        return vowels;
    }

    /**
     * If there are at least two identical consonant phonemes in a window, the
     * consonance count is incremented. The count is divided by the total number
     * of words in a name to obtain a consonanceFreq score.
     *
     * @param consonant
     * @return
     */
    public double calculateConsonance(char[] consonant) {
        double consonance = 0.0;

        for (int i = 0; i < consonant.length; i++) {
            char con1 = consonant[i];
            boolean b1 = isconsonant(con1);
            if (b1 == true) {
                for (int j = i + 1; j < consonant.length - 1; j++) {
                    char con2 = consonant[j];
                    boolean b2 = isconsonant(con2);
                    if (b2 == true) {

                        if (con1 == con2) {
                            consonance += 1;
                        }

                    }

                    //  }
                }
            }

        }

        //consonance frequency
        return consonance / consonant.length;
    }

    /**
     * If there are at least two identical assonance phonemes in a window, the
     * assonance count is incremented. The count is divided by the total number
     * of words in a name to obtain a consonanceFreq score for vowels.
     *
     * @param assonant
     * @return
     */
    public double calculateAssonance(char[] assonant) {
        double assonance = 0.0;

        for (int i = 0; i < assonant.length; i++) {
            char con1 = assonant[i];
            boolean b1 = isvowel(con1);
            if (b1 == true) {
                for (int j = i + 1; j < assonant.length - 1; j++) {
                    char con2 = assonant[j];
                    // if (con1 == null ? con2 == null : con1.equals(con2)) {
                    boolean b2 = isvowel(con2);
                    if (b2 == true) {

                        if (con1 == con2) {
                            assonance += 1;
                        }

                    }

                }
            }

        }

        //assonance frequency
        return assonance / assonant.length;
    }

    public int[] calculateHighLow(String character_name) {
        int high = 0;
        int low = 0;
        int k[] = new int[2];
        int b = 0;
        Espeak obj = new Espeak();
        ArrayList<String> phon_list = new ArrayList<String>();

        character_name = character_name.toLowerCase();
        character_name = character_name.replace("phd ", "");
        character_name = character_name.replace("msc ", "");
        character_name = character_name.replace(" msc", "");
        character_name = character_name.replace(" phd", "");
        character_name = character_name.replace(" dr", "");
        character_name = character_name.replace("dr ", "");
        character_name = character_name.replace(" sen", "");
        character_name = character_name.replace("sen ", "");

        character_name = character_name.replace(" sir", "");
        character_name = character_name.replace("sir ", "");
        character_name = character_name.replace(".", "");

        String s[] = character_name.split(" ");

        String tem = "";
        //int i = 0;
        for (String item : s) {
            b = 0;
            String command = "espeak -q --ipa " + item + " -v english";
            String output = Espeak.executeCommand(command);

            if (output.contains("a")) {
                low = low + 1;
            }
            if (output.contains("ɑ")) {
                low = low + 1;
            }
            if (output.contains("æ")) {
                low = low + 1;
            }
            if (output.contains("i")) {
                high = high + 1;
            }
            if (output.contains("y")) {
                high = high + 1;
            }
            if (output.contains("u")) {
                high = high + 1;
            }

//            if (output.length() > 1) {
//                String o1 = output.trim().substring(0, 2);
//                phon_list.add(o1);
//              
//                System.out.println("Output: " + o1);
//            }
        }
        k[0] = high;
        k[1] = low;
        return k;

    }

    /**
     * Alliteration narrowly refers to the repetition of a consonant in any
     * syllables that, according to the poem's meter, are stressed,[3][4][5] as
     * in James Thomson's verse "Come…dragging the lazy languid Line along".
     *
     * @return
     */
    public int calculateAlliteration(String character_name) {
        int b = 0;
        Espeak obj = new Espeak();
        ArrayList<String> phon_list = new ArrayList<String>();

        character_name = character_name.toLowerCase();
        character_name = character_name.replace("phd ", "");
        character_name = character_name.replace("msc ", "");
        character_name = character_name.replace(" msc", "");
        character_name = character_name.replace(" phd", "");
        character_name = character_name.replace(" dr", "");
        character_name = character_name.replace("dr ", "");
        character_name = character_name.replace(" sen", "");
        character_name = character_name.replace("sen ", "");

        character_name = character_name.replace(" sir", "");
        character_name = character_name.replace("sir ", "");
        character_name = character_name.replace(".", "");

        String s[] = character_name.split(" ");

        String tem = "";
        //int i = 0;
        for (String item : s) {
            b = 0;
            String command = "espeak -q --ipa " + item + " -v english";
            String output = Espeak.executeCommand(command);
            if (output.length() > 1) {
                String o1 = output.trim().substring(0, 2);
                phon_list.add(o1);
                //  tem = tem + o1;
                // if (output.contains(o1 + "'")) {
                //   phon_list.add(o1);
                // tem = tem + "/";
                // }
                // phon_list.add(tem);
                // tem = tem + "\n";
                System.out.println("Output: " + o1);
            }
        }
        System.out.println("Output: " + phon_list.size());
        // String t[] = tem.split("\n");
        for (int i = 0; i < phon_list.size(); i++) {
            String item1 = phon_list.get(i);
            for (int j = i + 1; j < phon_list.size(); j++) {
                String item2 = phon_list.get(j);
                System.out.println("Output1: " + item1);
                System.out.println("Output2: " + item2);
                if (item1.equals(item2)) {
                    System.out.println("Output: " + item1);
                    return 1;
                } else {
                    String command = "espeak -q --ipa " + s[j] + " -v english";
                    String output = Espeak.executeCommand(command);
                    if (output.contains(item1)) {
                        return 1;

                    }

                }
            }
        }

//        for (String phon_list1 : phon_list) {
//            String out = phon_list1.substring(0, 1);
//            for (int j = 1; j < phon_list.size(); j++) {
//                String out1 = phon_list.get(j).substring(0, 1);
//                if (out.equalsIgnoreCase(out1)) {
//
//                }
//            }
//        }
        return b;
    }

    public static boolean isvowel(char chr) {
        return (chr == 'A' || chr == 'E' || chr == 'I'
                || chr == 'O' || chr == 'U');
    }

    public static boolean isupper(char chr) {
        return (!(chr < 'A' || chr > 'Z'));
    }

    public static boolean islower(char chr) {
        return (!(chr < 'a' || chr > 'z'));
    }

    public static boolean isalpha(char chr) {
        return (isupper(chr) || islower(chr));
    }

    public static boolean isconsonant(char chr) {
        return (isupper(chr) && !isvowel(chr));
    }

    /**
     * Number of consonants in a given string
     *
     * @param name
     * @return
     */
    public int numOfConsonants(String name) {
        int consonants = 0;
        char c[] = name.toLowerCase().toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] != 'a' && c[i] != 'e' && c[i] != 'i'
                    && c[i] != 'o' && c[i] != 'u') {
                consonants++;
            }

        }

        return consonants;
    }

    public int numberOfPlosives(String name) {
        int plosives = 0;
        char[] phonemePlosives = {'\u0070', '\u0062', '\u0074', '\u0064', '\u006B', '\u006B', '\u0067'};
        char name_chars[] = toPhoneme(name).toCharArray();
        for (int i = 0; i < phonemePlosives.length; i++) {
            // test each in turn
            for (int j = 0; j < name_chars.length; j++) {
                if (name_chars[j] == phonemePlosives[i]) {
                    plosives++;
                }

            }
        }
        // not a plosive
        return plosives;
    }

    public int numberOfFricatives(String name) {
        int fricatives = 0;
        char name_chars[] = toPhoneme(name).toCharArray();
        char[] phonemeFricatives = {'\u0066', '\u0076', '\u03B8', '\u00F0', '\u0073', '\u007A', '\u0283', '\u0292', '\u0068', '\u02A7', '\u02A4'};

        for (int i = 0; i < phonemeFricatives.length; i++) {
            // test each in turn
            for (int j = 0; j < name_chars.length; j++) {
                if (name_chars[j] == phonemeFricatives[i]) {
                    fricatives++;
                }

            }

        }
        // not a fricative
        return fricatives;
    }

    public int numberOfAffricate(String name) {
        int affricates = 0;
        char name_chars[] = toPhoneme(name).toCharArray();

        char[] phonemeArtificate = {'\u02A7', '\u02A4'};
        //TextToPhonemes p = new TextToPhonemes();

        for (int i = 0; i < phonemeArtificate.length; i++) {
            // test each in turn
            for (int j = 0; j < name_chars.length; j++) {
              
                if (name_chars[j] == phonemeArtificate[i]) {
                    affricates++;
                }

            }

        }
        // not a fricative
        return affricates;
    }

    /**
     * CV factor specifying whether the initial phoneme of the word is a
     * consonant (C) or a vowel (V).
     *
     * @param name
     * @return
     */
    public String startCV(String name) {
        String cv = "";
        char c[] = name.toLowerCase().trim().toCharArray();
        if (c[0] != 'a' && c[0] != 'e' && c[0] != 'i'
                && c[0] != 'o' && c[0] != 'u') {
            cv = "consonant";
        } else {
            cv = "vowel";
        }

        return cv;

    }

    public int numberOfSubsequentPhonems(String character_name) {
        int num = 0;
        char c[] = toPhoneme(character_name).toCharArray();
        for (int i = 0; i < c.length; i++) {
            for (int j = 1; j < c.length - 1; j++) {
                if (c[i] == c[j]) {
                    num = num + 1;
                }

            }
        }

        return num;
    }

    /**
     * CV factor specifying whether the final phoneme of the word is a consonant
     * (C) or a vowel (V).
     *
     * @param name
     * @return
     */
    public String endCV(String name) {
        String cv = "";
        char c[] = name.toCharArray();
        if (c[c.length - 1] != 'a' && c[c.length - 1] != 'e' && c[c.length - 1] != 'i'
                && c[c.length - 1] != 'o' && c[c.length - 1] != 'u') {
            cv = "consonant";
        } else {
            cv = "vowel";
        }

        return cv;

    }

    /**
     * if starts with voiced or voiceless
     *
     * @param name
     * @return
     */
    public String startVoice(String name) {
        //vcd 0062
        char[] vcd = {'\u0062', '\u0064', '\u0067', '\u006A', '\u006C', '\u0076',
            '\u0077', '\u007A', '\u025F', '\u0292', '\u0263', '\u029F',
            '\u026B', '\u0262', '\u00F0', '\u03B2', '\u0263', '\u029F'};

        char[] vls = {'\u0063', '\u0066', '\u006B', '\u0070', '\u0071', '\u0073',
            '\u0074', '\u0078', '\u0283', '\u03A6', '\u026C', '\u0127',
            '\u00E7', '\u03C7'};

        String voice = "other";
        name = removeSpecialCharacters(name);
        name = name.toLowerCase();
        name = name.replace("phd ", "");
        name = name.replace("msc ", "");
        name = name.replace(" msc", "");
        name = name.replace(" phd", "");
        name = name.replace(" dr", "");
        name = name.replace("dr ", "");
        name = name.replace(" sen", "");
        name = name.replace("sen ", "");

        name = name.replace(" sir", "");
        name = name.replace("sir ", "");
        char c[] = name.trim().toCharArray();
        System.out.println("uuuu  " + name);
        // System.out.println("uuuu  " + c[0]);
        for (int i = 0; i < vcd.length; i++) {
            if (c[0] == vcd[i]) {
                System.out.println("uuuu  " + c[0]);
                return "voiced";
            }
        }

        for (int i = 0; i < vls.length; i++) {
            if (c[0] == vls[i]) {
                return "voiceless";
            }
        }

        return voice;

    }

//       Uvular: /ɴ / ("small cap N" U0274)
//Velar: /ŋ/ (U014B) -- identified with {nga.}
//Palatal: /ɲ/ (U0272) -- identified with {Ña.}
//Retroflex: /ɳ / (U0273) (with the retroflex hook) -- identified with {Na.}
//Alveolar or dental: /n/ (U006E) -- identified with {na.}
//Labio-dental: /ɱ/ (U0271) -- as with other labio-dentals /f, v/, this is not in Burmese-Myanmar
//Bilabial: /m/ (U006D)
//       
    public int numberOfNasals(String name) {
        int nasals = 0;
        char name_chars[] = toPhoneme(name).toCharArray();
        char[] phonemeNasal = {'\u014B', '\u0272', '\u006E', '\u0273', '\u006D', '\u0271'};

        for (int i = 0; i < phonemeNasal.length; i++) {
            // test each in turn
            for (int j = 0; j < name_chars.length; j++) {
                if (name_chars[j] == phonemeNasal[i]) {
                    //  System.out.println(name_chars[j]);
                    //  System.out.println(phonemeNasal[i]);
                    nasals++;
                }

            }

        }
        // not a fricative
        return nasals;
    }

    /**
     * Returns true if a name contains a title ( degree, honor etc)
     * http://en.wikipedia.org/wiki/British_degree_abbreviations
     * http://mediawiki.middlebury.edu/wiki/LIS/Name_Standards#Suffix
     *
     * @param name
     * @return
     */
    public int hasHounourTitle(ArrayList<String> name) {

        int b = 0;
        for (String name1 : name) {
            name1 = name1.trim();
            name1 = name1.replace(".", "");
            if (name1.equalsIgnoreCase("phd") || (name1.equalsIgnoreCase("msc"))
                    || (name1.equalsIgnoreCase("dr"))
                    || (name1.equalsIgnoreCase("sen"))
                    || (name1.equalsIgnoreCase("sir"))
                    || (name1.equalsIgnoreCase("mr"))
                    || (name1.equalsIgnoreCase("mrs"))
                    || (name1.equalsIgnoreCase("dean"))
                    || (name1.equalsIgnoreCase("professor"))
                    || (name1.equalsIgnoreCase("lecturer"))
                    || (name1.equalsIgnoreCase("ambassador"))
                    || (name1.equalsIgnoreCase("chief"))
                    || (name1.equalsIgnoreCase("colonel"))
                    || (name1.equalsIgnoreCase("Princess"))
                    || (name1.equalsIgnoreCase("king"))
                    || (name1.equalsIgnoreCase("count"))
                    || (name1.equalsIgnoreCase("Duke"))
                    || (name1.equalsIgnoreCase("judje"))
                    || (name1.equalsIgnoreCase("governor"))
                    || (name1.equalsIgnoreCase("Lord"))
                    || (name1.equalsIgnoreCase("Commander"))
                    || (name1.equalsIgnoreCase("Countess"))) {
                return 1;
            }

        }
        return 0;
    }

    /**
     * Returns a sentiment value based on sentiwordent using similiarty measures
     * ( soundex)
     *
     * @param name
     * @return
     */
    public double getSentimentConnection(ArrayList<String> name) {
        SentiWordnetJDBC j_obj = new SentiWordnetJDBC();
        double senti_word = 0.0;
        for (String name1 : name) {
            name1 = name1.trim();
            if (!name1.equals("")) {
                senti_word = senti_word + j_obj.calculateSoundexScore(name1);
            }
        }

        return (senti_word / 2);

    }

    /**
     * Returns a sentiment value based on sentiwordent using similiarty measures
     * ( soundex)
     *
     * @param name
     * @return
     */
    public double getSentimentLeveinsteinConnection(ArrayList<String> name) {
        SentiWordnetJDBC j_obj = new SentiWordnetJDBC();
        double senti_word = 0.0;
        for (String name1 : name) {
            name1 = name1.trim();
            if (!name1.equals("")) {
                senti_word = senti_word + j_obj.calculateLevenshteinScore(name1);
            }
        }

        return (senti_word / 2);

    }

    public String preprocessName(String name) {
        name = name.trim();
        name = name.toLowerCase();
        return name;
    }

    public int getRhymes(String firstWord, String lastWord) {
        int b = 0;

        // check if words are identical
        if (firstWord.equalsIgnoreCase(lastWord)) {
            return 1;
        }

        if (firstWord.length() <= lastWord.length()) { // word1 smaller - 1st arg

            if (Rhyme.endRhymePresent(firstWord, lastWord)) {
                return 1;

            } else {
                return 0;
            }
        } else { // word2 smaller - 1st arg

            if (Rhyme.endRhymePresent(firstWord, lastWord)) {
                return 1;

            } else {
                return 0;
            }

//            // special case for i=j (comparing line with itself for internal ryhmes or other intresting stuff)
//            if (i == j) {
//
//            };
        }

    }

}
