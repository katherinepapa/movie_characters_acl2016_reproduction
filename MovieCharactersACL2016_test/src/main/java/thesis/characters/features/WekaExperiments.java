package thesis.characters.features;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import thesis.characters.model.Result;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.Resample;

/**
 *
 * @author katherine papantoniou
 */
public class WekaExperiments {

    public static void main(String args[]) throws Exception {
        ArrayList<Result> result_list = new ArrayList<Result>();

        WekaExperiments w = new WekaExperiments();
        result_list.add(w.runJ48("data.arff"));
        result_list.add(w.runJ48("data_only_domain_features.arff"));
        result_list.add(w.runJ48("data_only_phonological_features.arff"));
        result_list.add(w.runJ48("data_without_phonological_features.arff"));
        result_list.add(w.runJ48("data_without_consonance_features.arff"));
        result_list.add(w.runJ48("data_without_domain_features.arff"));
        result_list.add(w.runJ48("data_without_emotion_features.arff"));
        result_list.add(w.runJ48("data_without_poetic_features.arff"));
        result_list.add(w.runJ48("data_without_social_features.arff"));

        w.printResults(result_list);

    }

    public void printResults(ArrayList<Result> result_list) throws IOException {
        FileWriter f = new FileWriter("J48_table");
        BufferedWriter bf = new BufferedWriter(f);

        //create header
        for (Result result_list1 : result_list) {
            String desc = result_list1.getDescription().replace("_", " ").replace("data", "").trim();
            String res = "\t" + result_list1.getW_recall() + "\t" + result_list1.getW_precision() + "\t" + result_list1.getW_fmeasure();
            System.out.println(desc + res);
            bf.write(desc + res + "\n");
        }

        bf.close();
        f.close();
    }

    public Result runJ48(String fileName) throws Exception {
        Result r_obj = new Result();
        ArffLoader loader = new ArffLoader();
        loader.setFile(new File(fileName));

        Instances data = loader.getDataSet();
        // setting class attribute if the data format does not provide this information
        // For example, the XRFF format saves the class attribute information as well
        if (data.classIndex() == -1) {
            data.setClassIndex(data.numAttributes() - 1);
        }
        Resample r = new Resample();
        r.setRandomSeed(1);
        r.setSampleSizePercent(100.0);
        r.setInputFormat(data);
        Instances dataFiltered = Filter.useFilter(data, r);

        if (dataFiltered.classIndex() == -1) {
            dataFiltered.setClassIndex(dataFiltered.numAttributes() - 1);
        }
        J48 classifier = new J48();
        classifier.setConfidenceFactor((float) 0.25);
        classifier.setMinNumObj(2);
        classifier.setSeed(1);
        classifier.setUnpruned(false);

        Evaluation eval = new Evaluation(dataFiltered);
        eval.crossValidateModel(classifier, dataFiltered, 10, new Random(1));

        //print
        System.out.println("-------------------------");
        System.out.println("Results for : " + fileName);
        System.out.println("Recall: " + eval.weightedRecall());
        System.out.println("Precision: " + eval.weightedPrecision());
        System.out.println("F-measure: " + eval.weightedFMeasure());
        System.out.println("-------------------------");
        r_obj.setW_fmeasure(eval.weightedFMeasure());
        r_obj.setW_precision(eval.weightedPrecision());
        r_obj.setW_recall(eval.weightedRecall());
        r_obj.setDescription(fileName);
        return r_obj;
    }

}
