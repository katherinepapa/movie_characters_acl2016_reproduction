package thesis.characters.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import thesis.characters.database.SentiWordnetJDBC;
import thesis.characters.model.SentiSynset;

/**
 *
 * @author katherine papantoniou
 */
public class SentiWordnetImporter {

   

    public void SWimport(String inputFilename) throws UnsupportedEncodingException, IOException {
        Utilities u_obj = new Utilities();
        InputStream in = getClass().getResourceAsStream("/files/" + inputFilename + ".txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

        String input = "";
        input = reader.readLine();
        do {
            String s[] = input.split("\t");
            SentiSynset obj = new SentiSynset();
            obj.setSw_pos(s[0]);
            obj.setPositive(Double.parseDouble(s[2]));
            obj.setNegative(Double.parseDouble(s[3]));
            obj.setGloss(s[5]);

            if (obj.getNegative() != 0 || obj.getPositive() != 0) {
                String words = s[4];
                String w[] = words.split("#");

                SentiWordnetJDBC sw_obj = new SentiWordnetJDBC();

                for (String w1 : w) {
                    obj.setSw_word(w1.trim());
                    boolean b = u_obj.isNumber(w1);

                    if (b == false) {
                        sw_obj.insert(obj);
                    }

                }
            }

            input = reader.readLine();

        } while (input != null);

    }

}
