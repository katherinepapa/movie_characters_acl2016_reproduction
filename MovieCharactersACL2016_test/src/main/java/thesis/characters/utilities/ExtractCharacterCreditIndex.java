package thesis.characters.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import thesis.characters.model.Credit;

/**
 * Extracts the credit list of a character
 *
 * @author katerinaki
 */
public class ExtractCharacterCreditIndex {

    public final String SUFFIX_URL = "fullcredits?ref_=tt_ov_st_sm";

    

    public ArrayList<Credit> extractCreditList(String movie_url, String movie_title) throws IOException {
        ArrayList<Credit> credit_list = new ArrayList<>();

        Document doc = Jsoup.connect(movie_url + SUFFIX_URL).timeout(0).get();
        Elements cast = doc.select("table.cast_list");
        int index = 0;
        Iterator<Element> c = cast.iterator();
        do {
          
            Elements e = c.next().getElementsByClass("character");
            Iterator<Element> els = e.iterator();
            index = 0;
            do {
                //  System.out.println("\n" + els.next().text() + "\n");
                Credit c_obj = new Credit();
                index++;
                c_obj.setCharacter_name(els.next().text());
             
                c_obj.setCredit_index(index);
                credit_list.add(c_obj);
            } while (els.hasNext());

        } while (c.hasNext());

        return credit_list;
    }

}
