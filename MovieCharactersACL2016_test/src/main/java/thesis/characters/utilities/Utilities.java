package thesis.characters.utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author katerina papantoniou
 */
public class Utilities {

   
    public static ArrayList<String> listFilesForFolder(final File folder) {
        ArrayList<String> file_list = new ArrayList<>();
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
               
                file_list.add(fileEntry.getName());

            }
        }

        return file_list;
    }

  

 

    /**
     * a simple regex pattern to check if a given string consists only with
     * numbers
     *
     * @param name
     * @return
     */
    public boolean isNumber(String name) {
        boolean b = false;

        Pattern p = Pattern.compile("\\d");
        Matcher m = p.matcher(name);
        while (m.find()) {
            return true;
        }
        return b;

    }

    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

    private static String trim(String s, int width) {
        if (s.length() > width) {
            return s.substring(0, width - 1) + ".";
        } else {
            return s;
        }
    }



    public static String keepLetters(String input) {
        String name = "";
        Pattern p = Pattern.compile("[^0-9]+");
        Matcher m = p.matcher(input);
        while (m.find()) {
            name = m.group();
        }
        return name;
    }

    public static String removeSpecialCharacters(String name) {
        //not regex because we want to keep foreign characters
        name = name.replaceAll("^", "");
        name = name.replaceAll("\\(", "");
        name = name.replaceAll("\\)", "");
        name = name.replaceAll("-", "");
        name = name.replaceAll("#", "");
        name = name.replaceAll("$", "");
        name = name.replaceAll("@", "");
        name = name.replaceAll("/", "");
        return name;
    }

    /**
     * Check if the input contains characters only from the English alphabet
     *
     * @param input
     * @return
     */
    public static boolean checkEnglishCharacters(String input) {
        boolean b = false;
        // \\d @ \\?
        Pattern p = Pattern.compile("([A-Z]|[a-z]|\\s|#|$|%|^|&|\\(|\\)|-|_|=|~|;|:||)+");
        Matcher m = p.matcher(input);
        while (m.matches()) {
            b = true;
          
            return b;
        }
        return b;
    }

    
}
