package thesis.characters.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author katherine papantoniou
 * @email papanton@ics.forth.gr
 */
public class ExtractMovieCategory {

    public ArrayList<String> predifined_categories = new ArrayList<>();

    ArrayList<String> categories = new ArrayList<>(
            Arrays.asList("Adventure", "Western", "Action", "Musical", "Mystery", "Animation", "Sci-Fi", "Biography", "Thriller", "Crime", "Drama", "Comedy", "Horror"));

    public static void main(String args[]) throws IOException, InterruptedException {
        ExtractMovieCategory e_obj = new ExtractMovieCategory();
        ArrayList<String> category = e_obj.extractCategory("http://www.imdb.com/title/tt0062622/?ref_=ttfc_fc_tt");
        ///  System.out.println(category.toString());

        for (int i = 0; i < category.size(); i++) {
            System.out.println(category.get(i));
        }
    }

    public ArrayList<String> extractCategory(String link) throws IOException, InterruptedException {

        ArrayList<String> category_list = new ArrayList<>();

        Document doc = Jsoup.connect(link).timeout(0).get();

        Elements genre_alt = doc.select("span.itemprop").attr("itemprop", "genre");
        ListIterator<Element> iter;
        iter = genre_alt.listIterator();
        do {
            String category = iter.next().text().trim();

            for (int i = 0; i < categories.size(); i++) {
                if (categories.get(i).equals(category)) {
                    category_list.add(category);
                }
            }
        } while (iter.hasNext());

        return category_list;
    }

}
