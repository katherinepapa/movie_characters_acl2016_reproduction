package thesis.characters.phonemes;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import static thesis.characters.utilities.Utilities.removeSpecialCharacters;

/**
 *
 * @author fileas
 */
public class Espeak {

    public static void main(String[] args) {

        Espeak obj = new Espeak();

        String name = "Julie James";

//        String output = obj.getPhonems("bridget");
//        System.out.println("--- " + output);
        String output = "";
        String command = "espeak -q   --ipa  \"" + name + "\"";
//
//        //in windows
//String command = "ping -n 3 " + domainName;
//        String output = obj.executeCommand(command);
//        System.out.println(output);
//        command = "espeak --voices ";
//        output = obj.executeCommand(command);
//        System.out.println(output);
//
//        command = "espeak -h ";
//        output = obj.executeCommand(command);
//        System.out.println(output);
//
//        //espeak -f example.txt -s 120 -v german
        // command = "espeak --ipa   " + "πιάτο" + " -v greek ";

        // command = "espeak  --ipa  " + "gem" + "    -v english";
        output = obj.executeCommand(command);
        output = output.replace("ˈ", "");

        System.out.println("Output: " + output);

        char o[] = output.toCharArray();
        int b = 0;
        ArrayList<String> str_list = new ArrayList<String>();
        for (int i = 0; i < o.length; i++) {
            b = 0;
            // System.out.println(o[i]);

            if (o[i] == 't') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'ʃ') {
                        str_list.add("tʃ");
                        i = i + 1;
                        b = 1;
                    }
                }
            } else if (o[i] == 'e') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'ɪ') {
                        str_list.add("eɪ");
                        i = i + 1;
                        b = 1;
                    }
                }
            } else if (o[i] == 'a') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'ʊ') {
                        str_list.add("aʊ");
                        i = i + 1;
                        b = 1;
                    }

                }
            } else if (o[i] == 'j') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'u') {
                        str_list.add("ju");
                        i = i + 1;
                        b = 1;
                    }

                }
            } else if (o[i] == 'ɔ') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'ɪ') {
                        str_list.add("ɔɪ");
                        i = i + 1;
                        b = 1;
                    }

                }
            } else if (o[i] == 'd') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'ʒ') {
                        str_list.add("dʒ");
                        i = i + 1;
                        b = 1;
                    }

                }
            } else {
                str_list.add(output.substring(i, i + 1));
                b = 1;
            }
            if (b == 0) {
                str_list.add(output.substring(i, i + 1));
            }

        }

        for (String str_list1 : str_list) {
            str_list1 = str_list1.trim();
            if (!str_list1.equals("")) {
                System.out.println("*" + str_list1);
            }
        }

    }

    public static ArrayList<String> getPhonemsList(String name) {
        Espeak obj = new Espeak();

        //  String name = "Bridget Jones";
        //   String output = obj.getPhonems("bridget");
        //   System.out.println("--- " + output);
        String command = "espeak  -q    --ipa  \"" + name + "\" -v english";
        String output = "";
        output = obj.executeCommand(command);
        output = output.replace("ˈ", "");

        System.out.println("Output: " + output);

        char o[] = output.toCharArray();
        int b = 0;
        ArrayList<String> str_list = new ArrayList<String>();
        for (int i = 0; i < o.length; i++) {
            b = 0;
            // System.out.println(o[i]);

            if (o[i] == 't') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'ʃ') {//
                        // str_list.add("tʃ"); //
                        str_list.add("1");
                        i = i + 1;
                        b = 1;
                    }
                }
            } else if (o[i] == 'e') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'ɪ') {
                        // str_list.add("eɪ");
                        str_list.add("2");
                        i = i + 1;
                        b = 1;
                    }
                }
            } else if (o[i] == 'a') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'ʊ') {
                        //  str_list.add("aʊ");
                        str_list.add("3");
                        i = i + 1;
                        b = 1;
                    }

                }
            } else if (o[i] == 'j') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'u') {
                        //  str_list.add("ju");
                        str_list.add("4");
                        i = i + 1;
                        b = 1;
                    }

                }
            } else if (o[i] == 'ɔ') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'ɪ') {
                        //  str_list.add("ɔɪ");
                        str_list.add("5");
                        i = i + 1;
                        b = 1;
                    }

                }
            } else if (o[i] == 'd') {
                if (i + 1 <= o.length) {
                    if (o[i + 1] == 'ʒ') {
                        // str_list.add("dʒ");
                        str_list.add("6");
                        i = i + 1;
                        b = 1;
                    }

                }
            } else {
                str_list.add(output.substring(i, i + 1));
                b = 1;
            }
            if (b == 0) {
                str_list.add(output.substring(i, i + 1));
            }

        }

        for (String str_list1 : str_list) {
            str_list1 = str_list1.trim();
            if (!str_list1.equals("")) {
                System.out.println("*" + str_list1);
            }
        }
        
        
     ArrayList<String> str_list1 = new ArrayList<String>();    
            for (String str_list2 : str_list) {
            str_list2 = str_list2.trim();
            if (!str_list2.equals("")) {
              str_list1.add(str_list2);
            }
        }
        
 System.out.println("**" + str_list1);
        return str_list1;

    }

    public String getPhonems(String name) {
        String phonem = "";
        //  name = removeSpecialCharacters(name);
        String s[] = name.split(" ");

        for (String item : s) {
            String command = "espeak    -x --sep=, --ipa  \"" + item.trim() + "\"";
            String output = executeCommand(command);
            phonem = phonem + output + " ";
        }
        phonem = phonem.substring(1, phonem.length());
        return phonem.trim();

    }

    public static String executeCommand(String command) {

        StringBuilder output = new StringBuilder();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }
}
