package thesis.characters.model;

/**
 *
 * @author katerina papantoniou
 */
public class Annotation {

    private int annotation_id;
    private String movie_title;
    private String movie_genre;
    private String imdb_url;
    private String annotator_name;
    private String character_name;
    private String year;
    private String tag;
    private int credit_index;

    /**
     * @return the annotation_id
     */
    public int getAnnotation_id() {
        return annotation_id;
    }

    /**
     * @param annotation_id the annotation_id to set
     */
    public void setAnnotation_id(int annotation_id) {
        this.annotation_id = annotation_id;
    }

    /**
     * @return the movie_title
     */
    public String getMovie_title() {
        return movie_title;
    }

    /**
     * @param movie_title the movie_title to set
     */
    public void setMovie_title(String movie_title) {
        this.movie_title = movie_title;
    }

    /**
     * @return the imdb_url
     */
    public String getImdb_url() {
        return imdb_url;
    }

    /**
     * @param imdb_url the imdb_url to set
     */
    public void setImdb_url(String imdb_url) {
        this.imdb_url = imdb_url;
    }

    /**
     * @return the annotator_name
     */
    public String getAnnotator_name() {
        return annotator_name;
    }

    /**
     * @param annotator_name the annotator_name to set
     */
    public void setAnnotator_name(String annotator_name) {
        this.annotator_name = annotator_name;
    }

    /**
     * @return the character_name
     */
    public String getCharacter_name() {
        return character_name;
    }

    /**
     * @param character_name the character_name to set
     */
    public void setCharacter_name(String character_name) {
        this.character_name = character_name;
    }

    /**
     * @return the tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @return the movie_genre
     */
    public String getMovie_genre() {
        return movie_genre;
    }

    /**
     * @param movie_genre the movie_genre to set
     */
    public void setMovie_genre(String movie_genre) {
        this.movie_genre = movie_genre;
    }

    /**
     * @return the credit_index
     */
    public int getCredit_index() {
        return credit_index;
    }

    /**
     * @param credit_index the credit_index to set
     */
    public void setCredit_index(int credit_index) {
        this.credit_index = credit_index;
    }

    @Override
    public String toString() {
        return this.getCharacter_name()
                + "\t" + this.getAnnotator_name()
                + "\t" + this.getTag()
                + "\t" + this.getMovie_title()
                + "\t" + this.getMovie_genre()
                + "\t" + this.getYear()
                + "\t" + this.getImdb_url();

    }

}
