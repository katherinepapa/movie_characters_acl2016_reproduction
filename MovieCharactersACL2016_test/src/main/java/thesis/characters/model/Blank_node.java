package thesis.characters.model;

import java.util.ArrayList;

/**
 *
 * @author katerina papantoniou
 */
public class Blank_node {
     private ArrayList<selector> selector=new ArrayList<selector>();

    /**
     * @return the selector
     */
    public ArrayList<selector> getSelector() {
        return selector;
    }

    /**
     * @param selector the selector to set
     */
    public void setSelector(ArrayList<selector> selector) {
        this.selector = selector;
    }
}
