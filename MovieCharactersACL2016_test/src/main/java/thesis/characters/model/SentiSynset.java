package thesis.characters.model;

/**
 *
 * @author katerina papantoniou
 */
public class SentiSynset {

    private int sw_id;
    private double positive;
    private double negative;
    private String sw_word;
    private String sw_pos;
    private String gloss;

    /**
     * @return the sw_id
     */
    public int getSw_id() {
        return sw_id;
    }

    /**
     * @param sw_id the sw_id to set
     */
    public void setSw_id(int sw_id) {
        this.sw_id = sw_id;
    }

    /**
     * @return the positive
     */
    public double getPositive() {
        return positive;
    }

    /**
     * @param positive the positive to set
     */
    public void setPositive(double positive) {
        this.positive = positive;
    }

    /**
     * @return the negative
     */
    public double getNegative() {
        return negative;
    }

    /**
     * @param negative the negative to set
     */
    public void setNegative(double negative) {
        this.negative = negative;
    }

    /**
     * @return the sw_word
     */
    public String getSw_word() {
        return sw_word;
    }

    /**
     * @param sw_word the sw_word to set
     */
    public void setSw_word(String sw_word) {
        this.sw_word = sw_word;
    }

    /**
     * @return the sw_pos
     */
    public String getSw_pos() {
        return sw_pos;
    }

    /**
     * @param sw_pos the sw_pos to set
     */
    public void setSw_pos(String sw_pos) {
        this.sw_pos = sw_pos;
    }

    /**
     * @return the gloss
     */
    public String getGloss() {
        return gloss;
    }

    /**
     * @param gloss the gloss to set
     */
    public void setGloss(String gloss) {
        this.gloss = gloss;
    }

}
