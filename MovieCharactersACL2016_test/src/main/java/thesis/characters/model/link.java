package thesis.characters.model;

/**
 *
 * @author katerina papantoniou
 */
public class link {
    private String href;
  private String rel;
    /**
     * @return the href
     */
    public String getHref() {
        return href;
    }

    /**
     * @param href the href to set
     */
    public void setHref(String href) {
        this.href = href;
    }

//    /**
//     * @return the rel
//     */
//    public String getRel() {
//        return rel;
//    }
//
//    /**
//     * @param rel the rel to set
//     */
//    public void setRel(String rel) {
//        this.rel = rel;
//    }
//
//    /**
//     * @return the type
//     */
//    public String getType() {
//        return type;
//    }
//
//    /**
//     * @param type the type to set
//     */
//    public void setType(String type) {
//        this.type = type;
//    }

    /**
     * @return the rel
     */
    public String getRel() {
        return rel;
    }

    /**
     * @param rel the rel to set
     */
    public void setRel(String rel) {
        this.rel = rel;
    }
}
