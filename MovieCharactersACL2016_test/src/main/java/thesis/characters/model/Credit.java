package thesis.characters.model;

/**
 * Model class for credit index
 * @author katerina papantoniou
 */
public class Credit {
    private String movie_title;
    private String character_name;
    private int credit_index;

    /**
     * @return the character_name
     */
    public String getCharacter_name() {
        return character_name;
    }

    /**
     * @param character_name the character_name to set
     */
    public void setCharacter_name(String character_name) {
        this.character_name = character_name;
    }

    /**
     * @return the credit_index
     */
    public int getCredit_index() {
        return credit_index;
    }

    /**
     * @param credit_index the credit_index to set
     */
    public void setCredit_index(int credit_index) {
        this.credit_index = credit_index;
    }

    /**
     * @return the movie_title
     */
    public String getMovie_title() {
        return movie_title;
    }

    /**
     * @param movie_title the movie_title to set
     */
    public void setMovie_title(String movie_title) {
        this.movie_title = movie_title;
    }
    
}
