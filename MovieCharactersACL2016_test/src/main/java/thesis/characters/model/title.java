
package thesis.characters.model;

import java.util.ArrayList;

/**
 *
 * @author fileas
 */
public class title {
    private String title;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

}
