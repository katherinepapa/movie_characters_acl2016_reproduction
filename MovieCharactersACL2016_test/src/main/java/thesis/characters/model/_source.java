package thesis.characters.model;

import java.util.ArrayList;

/**
 *
 * @author katerina papantoniou
 */
public class _source {
    private ArrayList<String> url = new ArrayList<String>();
    private String text;
    private ArrayList<target> target = new ArrayList<>();
    private document document;
    private String user;

    /**
     * @return the url
     */
    public ArrayList<String> getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(ArrayList<String> url) {
        this.url = url;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the target
     */
    public ArrayList<target> getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(ArrayList<target> target) {
        this.target = target;
    }

    /**
     * @return the document
     */
    public document getDocument() {
        return document;
    }

    /**
     * @param document the document to set
     */
    public void setDocument(document document) {
        this.document = document;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

}
