package thesis.characters.model;

import java.util.ArrayList;

/**
 *
 * @author katerina papantoniou
 */
public class document {

    private ArrayList<link> link = new ArrayList<>();
    private String title;

    /**
     * @return the link
     */
    public ArrayList<link> getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(ArrayList<link> link) {
        this.link = link;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
