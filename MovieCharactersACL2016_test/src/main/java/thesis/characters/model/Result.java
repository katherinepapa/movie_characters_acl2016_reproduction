package thesis.characters.model;

/**
 *
 * @author katherine papantoniou
 */
public class Result {

    private String description;
    private double w_recall;
    private double w_precision;
    private double w_fmeasure;

    /**
     * @return the w_recall
     */
    public double getW_recall() {
        return w_recall;
    }

    /**
     * @param w_recall the w_recall to set
     */
    public void setW_recall(double w_recall) {
        this.w_recall = w_recall;
    }

    /**
     * @return the w_precision
     */
    public double getW_precision() {
        return w_precision;
    }

    /**
     * @param w_precision the w_precision to set
     */
    public void setW_precision(double w_precision) {
        this.w_precision = w_precision;
    }

    /**
     * @return the w_fmeasure
     */
    public double getW_fmeasure() {
        return w_fmeasure;
    }

    /**
     * @param w_fmeasure the w_fmeasure to set
     */
    public void setW_fmeasure(double w_fmeasure) {
        this.w_fmeasure = w_fmeasure;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

}
