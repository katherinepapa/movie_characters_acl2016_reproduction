package thesis.characters.model;

/**
 *
 * @author katerina papantoniou
 */
public class Item {

    private String _index;
    private String _type;
    private String _id;
    private int _score;
    private _source _source;

   //{"_index":"annotator","_type":"annotation","_id":"AUoxRo0w_BihQT6ASQoP","_score":0,"_source":{"tags":[],"document":{"dc":{},"twitter":{},"title":"Hypothesis","reply_to":[],"eprints":{},"link":[{"href":"http://83.212.97.155:5000/"}],"facebook":{},"highwire":{},"prism":{}},"text":"katerina","created":"2014-12-09T22:58:34.922851+00:00","updated":"2014-12-09T22:58:34.922925+00:00","permissions":{"update":["acct:123456@83.212.97.155"],"admin":["acct:123456@83.212.97.155"],"delete":["acct:123456@83.212.97.155"],"read":["acct:123456@83.212.97.155"]},"target":[{"selector":[{"endOffset":89,"startContainer":"/div[1]/article[1]/section[1]/p[1]","endContainer":"/div[1]/article[1]/section[1]/p[1]","type":"RangeSelector","startOffset":0},{"start":57,"type":"TextPositionSelector","end":146},{"prefix":"peer reviewed. Getting started","exact":"Great, you've got the server up and running, you're now ready to start kicking the tires.","type":"TextQuoteSelector","suffix":"Create an account using the sid"}],"source":"http://83.212.97.155:5000/","pos":{"height":21,"top":181.89999389648438}}],"consumer":"00000000-0000-0000-0000-000000000000","uri":"http://83.212.97.155:5000/","user":"acct:123456@83.212.97.155"}}
    /**
     * @return the _index
     */
    public String getIndex() {
        return _index;
    }

    /**
     * @param _index the _index to set
     */
    public void setIndex(String _index) {
        this._index = _index;
    }

    /**
     * @return the _type
     */
    public String getType() {
        return _type;
    }

    /**
     * @param _type the _type to set
     */
    public void setType(String _type) {
        this._type = _type;
    }

    /**
     * @return the _id
     */
    public String getId() {
        return _id;
    }

    /**
     * @param _id the _id to set
     */
    public void setId(String _id) {
        this._id = _id;
    }

    /**
     * @return the _score
     */
    public int getScore() {
        return _score;
    }

    /**
     * @param _score the _score to set
     */
    public void setScore(int _score) {
        this._score = _score;
    }

    /**
     * @return the _source
     */
    public _source getSource() {
        return _source;
    }

    /**
     * @param _source the _source to set
     */
    public void setSource(_source _source) {
        this._source = _source;
    }

}
