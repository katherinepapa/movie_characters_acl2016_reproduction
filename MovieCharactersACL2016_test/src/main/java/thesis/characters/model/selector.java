package thesis.characters.model;

/**
 *
 * @author katerina papantoniou
 */
public class selector {
   private String type;
   private String exact;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the exact
     */
    public String getExact() {
        return exact;
    }

    /**
     * @param exact the exact to set
     */
    public void setExact(String exact) {
        this.exact = exact;
    }

}
