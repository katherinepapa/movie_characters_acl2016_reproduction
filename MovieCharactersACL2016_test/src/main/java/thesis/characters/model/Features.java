package thesis.characters.model;

/**
 *
 * @author katerina papantoniou
 */
public class Features {

    //1. NumSyllables
    //2. NumWords
    //3. NumOfVowels
    //4. Consonance
    //5. Assonance
    //6. NumOfConsonants
    //7. NumberOfPlosives
    //8. NumberOfFricatives
    //9. NumberOfAffricate
    //10. NumberOfNasals
    //11. Rhyme
    //12. ForeignSuffix
    //13. startcv
    //14. endcv
    //15. voice
    //16. numberOfSubsequenPhonems
    //17. gender
    //18. alliteration
    //19. high
    //20. low
    private int numsyllabes;
    private int numwords;
    private int numvowels;
    private int numconsonants;
    private int numplosives;
    private int numfricatives;
    private int numaffricate;
    private int numnasals;
    private int foreign;
    private int sameastitle;
    private int article;
    private String genre;
    private int rhyme;
    private double freqconsonance;
    private double freqassonance;
    private int honourTitle;
    private int creditIndex;
    private double sentimentconnection;//soundex
    private double sentimentLevenshteinconnection;
    private int foreignsuffix;
    private double first_name_freq;
    private double last_name_freq;
    private double full_name_freq;
    private String startcv;
    private String endcv;
    private String voice;
    private int numsubsequent;
    private int sameuppercase;
    private String gender;
    private int alliteration;
    private int high;
    private int low;

    /**
     * @return the numsyllabes
     */
    public int getNumsyllabes() {
        return numsyllabes;
    }

    /**
     * @param numsyllabes the numsyllabes to set
     */
    public void setNumsyllabes(int numsyllabes) {
        this.numsyllabes = numsyllabes;
    }

    /**
     * @return the numwords
     */
    public int getNumwords() {
        return numwords;
    }

    /**
     * @param numwords the numwords to set
     */
    public void setNumwords(int numwords) {
        this.numwords = numwords;
    }

    /**
     * @return the numvowels
     */
    public int getNumvowels() {
        return numvowels;
    }

    /**
     * @param numvowels the numvowels to set
     */
    public void setNumvowels(int numvowels) {
        this.numvowels = numvowels;
    }

    /**
     * @return the numconsonants
     */
    public int getNumconsonants() {
        return numconsonants;
    }

    /**
     * @param numconsonants the numconsonants to set
     */
    public void setNumconsonants(int numconsonants) {
        this.numconsonants = numconsonants;
    }

    /**
     * @return the numplosives
     */
    public int getNumplosives() {
        return numplosives;
    }

    /**
     * @param numplosives the numplosives to set
     */
    public void setNumplosives(int numplosives) {
        this.numplosives = numplosives;
    }

    /**
     * @return the numfricatives
     */
    public int getNumfricatives() {
        return numfricatives;
    }

    /**
     * @param numfricatives the numfricatives to set
     */
    public void setNumfricatives(int numfricatives) {
        this.numfricatives = numfricatives;
    }

    /**
     * @return the numaffricate
     */
    public int getNumaffricate() {
        return numaffricate;
    }

    /**
     * @param numaffricate the numaffricate to set
     */
    public void setNumaffricate(int numaffricate) {
        this.numaffricate = numaffricate;
    }

    /**
     * @return the numnasals
     */
    public int getNumnasals() {
        return numnasals;
    }

    /**
     * @param numnasals the numnasals to set
     */
    public void setNumnasals(int numnasals) {
        this.numnasals = numnasals;
    }

    /**
     * @return the rhyme
     */
    public int isRhyme() {
        return rhyme;
    }

    /**
     * @param rhyme the rhyme to set
     */
    public void setRhyme(int rhyme) {
        this.rhyme = rhyme;
    }

    /**
     * @return the freqconsonance
     */
    public double getFreqconsonance() {
        return freqconsonance;
    }

    /**
     * @param freqconsonance the freqconsonance to set
     */
    public void setFreqconsonance(double freqconsonance) {
        this.freqconsonance = freqconsonance;
    }

    /**
     * @return the freqassonance
     */
    public double getFreqassonance() {
        return freqassonance;
    }

    /**
     * @param freqassonance the freqassonance to set
     */
    public void setFreqassonance(double freqassonance) {
        this.freqassonance = freqassonance;
    }

    @Override
    public String toString() {
        return null;
    }

    /**
     * @return the honourTitle
     */
    public int getHonourTitle() {
        return honourTitle;
    }

    /**
     * @param honourTitle the honourTitle to set
     */
    public void setHonourTitle(int honourTitle) {
        this.honourTitle = honourTitle;
    }

    /**
     * @return the sentimentconnection
     */
    public double getSentimentconnection() {
        return sentimentconnection;
    }

    /**
     * @param sentimentconnection the sentimentconnection to set
     */
    public void setSentimentconnection(double sentimentconnection) {
        this.sentimentconnection = sentimentconnection;
    }

    /**
     * @return the sentimentLevenshteinconnection
     */
    public double getSentimentLevenshteinconnection() {
        return sentimentLevenshteinconnection;
    }

    /**
     * @param sentimentLevenshteinconnection the sentimentLevenshteinconnection
     * to set
     */
    public void setSentimentLevenshteinconnection(double sentimentLevenshteinconnection) {
        this.sentimentLevenshteinconnection = sentimentLevenshteinconnection;
    }

    /**
     * @return the foreign
     */
    public int getForeign() {
        return foreign;
    }

    /**
     * @param foreign the foreign to set
     */
    public void setForeign(int foreign) {
        this.foreign = foreign;
    }

    /**
     * @return the sameastitle
     */
    public int getSameastitle() {
        return sameastitle;
    }

    /**
     * @param sameastitle the sameastitle to set
     */
    public void setSameastitle(int sameastitle) {
        this.sameastitle = sameastitle;
    }

    /**
     * @return the article
     */
    public int getArticle() {
        return article;
    }

    /**
     * @param article the article to set
     */
    public void setArticle(int article) {
        this.article = article;
    }

    /**
     * @return the genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * @param genre the genre to set
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * @return the creditIndex
     */
    public int getCreditIndex() {
        return creditIndex;
    }

    /**
     * @param creditIndex the creditIndex to set
     */
    public void setCreditIndex(int creditIndex) {
        this.creditIndex = creditIndex;
    }

    /**
     * @return the foreignsuffix
     */
    public int getForeignsuffix() {
        return foreignsuffix;
    }

    /**
     * @param foreignsuffix the foreignsuffix to set
     */
    public void setForeignsuffix(int foreignsuffix) {
        this.foreignsuffix = foreignsuffix;
    }

    /**
     * @return the first_name_freq
     */
    public double getFirst_name_freq() {
        return first_name_freq;
    }

    /**
     * @param first_name_freq the first_name_freq to set
     */
    public void setFirst_name_freq(double first_name_freq) {
        this.first_name_freq = first_name_freq;
    }

    /**
     * @return the last_name_freq
     */
    public double getLast_name_freq() {
        return last_name_freq;
    }

    /**
     * @param last_name_freq the last_name_freq to set
     */
    public void setLast_name_freq(double last_name_freq) {
        this.last_name_freq = last_name_freq;
    }

    /**
     * @return the full_name_freq
     */
    public double getFull_name_freq() {
        return full_name_freq;
    }

    /**
     * @param full_name_freq the full_name_freq to set
     */
    public void setFull_name_freq(double full_name_freq) {
        this.full_name_freq = full_name_freq;
    }

    /**
     * @return the startcv
     */
    public String getStartcv() {
        return startcv;
    }

    /**
     * @param startcv the startcv to set
     */
    public void setStartcv(String startcv) {
        this.startcv = startcv;
    }

    /**
     * @return the endcv
     */
    public String getEndcv() {
        return endcv;
    }

    /**
     * @param endcv the endcv to set
     */
    public void setEndcv(String endcv) {
        this.endcv = endcv;
    }

    /**
     * @return the voice
     */
    public String getVoice() {
        return voice;
    }

    /**
     * @param voice the voice to set
     */
    public void setVoice(String voice) {
        this.voice = voice;
    }

    /**
     * @return the numsubsequent
     */
    public int getNumsubsequent() {
        return numsubsequent;
    }

    /**
     * @param numsubsequent the numsubsequent to set
     */
    public void setNumsubsequent(int numsubsequent) {
        this.numsubsequent = numsubsequent;
    }

    /**
     * @return the sameuppercase
     */
    public int getSameuppercase() {
        return sameuppercase;
    }

    /**
     * @param sameuppercase the sameuppercase to set
     */
    public void setSameuppercase(int sameuppercase) {
        this.sameuppercase = sameuppercase;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the alliteration
     */
    public int getAlliteration() {
        return alliteration;
    }

    /**
     * @param alliteration the alliteration to set
     */
    public void setAlliteration(int alliteration) {
        this.alliteration = alliteration;
    }

    /**
     * @return the high
     */
    public int getHigh() {
        return high;
    }

    /**
     * @param high the high to set
     */
    public void setHigh(int high) {
        this.high = high;
    }

    /**
     * @return the low
     */
    public int getLow() {
        return low;
    }

    /**
     * @param low the low to set
     */
    public void setLow(int low) {
        this.low = low;
    }

}
