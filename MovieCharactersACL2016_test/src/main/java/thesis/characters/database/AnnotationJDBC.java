package thesis.characters.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import thesis.characters.model.Annotation;

/**
 * retrieve information for the Annotation table
 *
 * @author fileas
 */
public class AnnotationJDBC {

    public ArrayList<String> getCharacterNames() {
        DatabaseConnection dataSource = new DatabaseConnection();
        String query = null;
        Connection conn = null;
        ArrayList<String> annotation_list = new ArrayList<>();
        String character = "";
        try {

            conn = dataSource.getConnection();
///????????????distinct
            query = "SELECT distinct  character_name,tag FROM annotation ORDER BY tag";

            try (
                    PreparedStatement ps = conn.prepareStatement(query)) {
                ResultSet rs;
                rs = ps.executeQuery();
                rs.next();

                do {
                    character = rs.getObject(1).toString();
                    annotation_list.add(character);

                } while (rs.next() != false);

            }
            return annotation_list;

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }

    }

    public ArrayList<Annotation> getAnnotation(String character_name) {
        DatabaseConnection dataSource = new DatabaseConnection();
        String query;
        Connection conn = null;
        ArrayList<Annotation> annotation_list = new ArrayList<>();

        try {

            conn = dataSource.getConnection();

            query = "SELECT annotation_id, movie_title, imdb_url, annotator_name, character_name,  movie_year, movie_genre, tag, character_index"
                    + "  FROM annotation WHERE character_name = '" + character_name + "'";
            try (PreparedStatement ps = conn.prepareStatement(query)) {
                ResultSet rs;
                rs = ps.executeQuery();

                do {
                    boolean b = rs.next();
                    if (b == true) {
                        Annotation a = new Annotation();
                        a.setAnnotation_id(Integer.parseInt(rs.getObject(1).toString()));
                        a.setMovie_title(rs.getObject(2).toString());
                        a.setImdb_url(rs.getObject(3).toString());
                        a.setAnnotator_name(rs.getObject(4).toString());
                        a.setCharacter_name(rs.getObject(5).toString());
                        a.setYear(rs.getObject(6).toString());
                        a.setMovie_genre(rs.getObject(7).toString());
                        a.setTag(rs.getObject(8).toString());
                        a.setCredit_index(rs.getInt("character_index"));
                        annotation_list.add(a);
                    }

                } while (rs.next() != false);
                // System.out.println(ps.executeUpdate());
            }
            return annotation_list;

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }

    }

    public int insert(thesis.characters.model.Annotation c_obj) {
        DatabaseConnection dataSource = new DatabaseConnection();
        String query = null;
        Connection conn = null;
        try {

            conn = dataSource.getConnection();

            query = "INSERT INTO annotation(movie_title,imdb_url,annotator_name,character_name,movie_year,movie_genre,tag,character_index)"
                    + " VALUES ('" + c_obj.getMovie_title().replace("'", "") + "' ,'" + c_obj.getImdb_url() + "', '" + c_obj.getAnnotator_name() + "' ,   '" + c_obj.getCharacter_name() + "','" + c_obj.getYear() + "'," + "'" + c_obj.getMovie_genre() + "','" + c_obj.getTag() + "'," + c_obj.getCredit_index() + " )  RETURNING annotation_id;";

            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs;
            rs = ps.executeQuery();
            rs.next();
            int last_inserted_id = -5;
            do {

                last_inserted_id = Integer.parseInt(rs.getObject(1).toString());

            } while (rs.next() != false);

            ps.close();
            return last_inserted_id;

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }
    }
}
