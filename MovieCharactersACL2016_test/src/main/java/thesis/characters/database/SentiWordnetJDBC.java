package thesis.characters.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import thesis.characters.model.SentiSynset;

/**
 * methods for the sentiwordnet table in DB
 * @author ICS - FORTH
 */
public class SentiWordnetJDBC {

    public int insert(thesis.characters.model.SentiSynset c_obj) {
        DatabaseConnection dataSource = new DatabaseConnection();
        String query = null;
        Connection conn = null;
        try {

            conn = dataSource.getConnection();
            query = "INSERT INTO annotation_imdb.sentiwordnet"
                    + "(positive,negative,sw_word,sw_pos,"
                    + "gloss)"
                    + " VALUES (" + c_obj.getPositive() + ","
                    + "" + c_obj.getNegative() + ",'" + c_obj.getSw_word().replace("'", "")
                    + "'" + ",'" + c_obj.getSw_pos()
                    + " ', '" + c_obj.getGloss().replace("'", "") + "') RETURNING sw_id;";

            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs;
            rs = ps.executeQuery();
            rs.next();
            int last_inserted_id = -5;
            do {

                last_inserted_id = Integer.parseInt(rs.getObject(1).toString());
                // rs.next();

            } while (rs.next() != false);

            ps.close();
            return last_inserted_id;

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }
    }

    public ArrayList<SentiSynset> getSoundex(String word) {
        ArrayList<SentiSynset> list = new ArrayList<>();
        DatabaseConnection dataSource = new DatabaseConnection();
        String query = null;
        Connection conn = null;
        try {

            conn = dataSource.getConnection();
            query = "SELECT * FROM sentiwordnet WHERE soundex(sw_word) = soundex('" + word + "');";

            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs;
            rs = ps.executeQuery();
            //  rs.next();

            while (!rs.next()) {
                SentiSynset s_obj = new SentiSynset();
                s_obj.setGloss(rs.getString("gloss"));
                s_obj.setNegative(rs.getInt("negative"));
                s_obj.setPositive(rs.getInt("positive"));
                s_obj.setSw_word(rs.getString("sw_word"));
                s_obj.setSw_pos(rs.getString("sw_pos"));
                s_obj.setSw_id(rs.getInt("sw_id"));

                list.add(s_obj);

                rs.next();

            }

            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }

        return list;
    }

    /**
     * Weighted average of soundex
     *
     * @param word
     * @return
     */
    public double calculateSoundexScore(String word) {
        ArrayList<SentiSynset> list = new ArrayList<>();
        double sound_weight = 0.0;
        DatabaseConnection dataSource = new DatabaseConnection();
        String query = null;
        Connection conn = null;
        try {

            conn = dataSource.getConnection();
            query = "SELECT gloss, sw_word, negative, positive, sw_id, sw_pos FROM sentiwordnet WHERE soundex(sw_word) = soundex('" + word + "');";

            try (PreparedStatement ps = conn.prepareStatement(query)) {
                ResultSet rs;
                rs = ps.executeQuery();

                int index = 0;
                int weight = 1;
                double sound_negative = 0.0;
                double sound_positive = 0.0;
                if (rs != null) {
                    while (rs.next()) {
                        if (!rs.wasNull()) {

                            SentiSynset s_obj = new SentiSynset();
                            s_obj.setGloss(rs.getString("gloss"));
                            s_obj.setNegative(rs.getDouble("negative"));
                            s_obj.setPositive(rs.getDouble("positive"));
                            s_obj.setSw_word(rs.getString("sw_word"));
                            s_obj.setSw_pos(rs.getString("sw_pos"));
                            s_obj.setSw_id(rs.getInt("sw_id"));
                           // if (rs.getString("sw_pos").equals("n")) {

                                sound_negative = sound_negative + (weight * s_obj.getNegative());

                                sound_positive = sound_positive + (weight * s_obj.getPositive());
                                weight = weight / 2;
                                list.add(s_obj);
                            //}
                            //  rs.next();
                        }
                    }
                }

                if (sound_negative > sound_positive) {
                    sound_weight = -sound_negative;
                } else {
                    sound_weight = sound_positive;
                }

                //sound_weight = (sound_negative + sound_positive) / 2;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }

        return sound_weight;
    }

    /**
     * Weighted average of levenshtein
     *
     * @param word
     * @return
     */
    public double calculateLevenshteinScore(String word) {
        ArrayList<SentiSynset> list = new ArrayList<>();
        double sound_weight = 0.0;
        DatabaseConnection dataSource = new DatabaseConnection();
        String query = null;
        Connection conn = null;
        try {

            conn = dataSource.getConnection();
            query = "SELECT levenshtein('" + word + "', sw_word)  as distance ,gloss, sw_word, negative, positive, sw_id, sw_pos FROM sentiwordnet as s\n"
                    + "order by distance\n"
                    + "limit 3";
            try (PreparedStatement ps = conn.prepareStatement(query)) {
                ResultSet rs;
                rs = ps.executeQuery();

                int index = 0;
                int weight = 1;
                double sound_negative = 0.0;
                double sound_positive = 0.0;
                if (rs != null) {
                    while (rs.next()) {
                        if (!rs.wasNull()) {

                            SentiSynset s_obj = new SentiSynset();
                            s_obj.setGloss(rs.getString("gloss"));
                            s_obj.setNegative(rs.getDouble("negative"));
                            s_obj.setPositive(rs.getDouble("positive"));
                            s_obj.setSw_word(rs.getString("sw_word"));
                            s_obj.setSw_pos(rs.getString("sw_pos"));
                            s_obj.setSw_id(rs.getInt("sw_id"));
                           // if (rs.getString("sw_pos").equals("n")) {
                                sound_negative = sound_negative + (weight * s_obj.getNegative());
                                sound_positive = sound_positive + (weight * s_obj.getPositive());
                                weight = weight / 2;
                                list.add(s_obj);
                           // }

                            //  rs.next();
                        }
                    }
                }

                if (sound_negative > sound_positive) {
                    sound_weight = -sound_negative;
                } else {
                    sound_weight = sound_positive;
                }

                // sound_weight = (sound_negative + sound_positive) / 2;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }

        return sound_weight;
    }

}
