/***********************************************************************
Authors: Katherine Papantoniou, Stasinos Konstantopoulos
Contact Email: papanton@ics.forth.gr, konstant@iit.demokritos.gr
Date: 30 - 10 - 2017
Description: Accompanying software for the paper:
"Unravelling Names of Fictional Characters"
In: Proceedings of ACL 2016, Berlin, 7 - 12 August 2016
URL: https://aclweb.org/anthology/P/P16/P16-1203.pdf
************************************************************************/

Prerequisites:
 - Java version 1.7 or newer.
 - Maven build system.
 - PostgreSQL version 9.2 or newer.
 - SentiWordnet version 3.0 [1]
 - Hypothes.is, if a new annotation campaign will be run [2]
 - eSpeak [3]

Index
1. General description of the accompanied software
2. Instructions/description for the setup of the manual annotation task
3. Description of the database
4. Generating Weka experimental setups:
   Description of the application that
   a. parses the results from the annotation step
   b. creates a series of Weka arff files
5. Reproducing the ACL 2016 Experiments
6. References


1. General
------------------------------------------------------------------------

This repository contains the complete setup for the experiments
presented by Papantoniou and Konstantopoulos [4].

The setup consists of:

 - The annotation guidelines used to elicit the positive/negative
   characterization of movie characters. The resulting raw annotations
   are provided in JSON format.

 - A PostgreSQL database dump which includes the raw annotations as
   well as metadata about the movies [4, Sect. 3.5 "Domain Features].

 - A Java application which enriches movies data with SentiWordNet
   [4, Sect. 3.1 "Emotions"]. The Java application also extracts from
   the database the arff files used for the experiments.

 - The arff files used for the experiments presented in the paper
   [4, Sect. 5 "Discussion of Results"].

 
2. Hypothes.is
------------------------------------------------------------------------

For the annotation task we setup the Hypothes.is (h project) in a
Linux server and we tested the annotation procedure with the help of
a small number of volunteers. We then sent invitations via email and
Facebook to attract annotators for the main annotation campaign.

Those who accepted our request were sent detailed annotation
guidelines, the list of the available movies for annotation and a two
weeks' time to complete their annotations. The annotation guidelines
(in Greek and in English) can be found inside the directory
"Guidelines". The process was fairly simple and the only software
requirement was that the volunteers install the Hypothes.is extension
in their browser.

The annotation results extracted in a form of a json file. You can
find the json file under the path src/main/resources/files in a raw
and pretty json format (annotations.json and annotations_pretty.json).


3. Database "annotation_imdb"
------------------------------------------------------------------------

The application makes use of the annotation_imdb schema that has one
table called "Annotation". We provide the dump
(annotation_imdb.annotation) for the database schema and data
in the "Database" directory.

Also, you must create a table for the Sentiwordnet with the SQL
command below:

CREATE TABLE annotation_imdb.sentiwordnet
(
  sw_id serial NOT NULL,
  positive real,
  negative real,
  sw_word text,
  sw_pos text,
  gloss text,
  CONSTRAINT sentiwordnet_pkey PRIMARY KEY (sw_id)
)

In addition the extension fuzzystrmatch should be installed by typing:
CREATE EXTENSION fuzzystrmatch

The Sentiwordnet data will be imported using the MovieCharactersACL2016
application (step 4 below.)

Note: We recommend to run the database schema
(annotation_imdb.annotation) through the psql terminal. If you want to
use a graphical environment e.g., postgresql workbench modify the COPY
command accordingly.

You can run the database schema from psql by:
\connect annotation_imdb
\i your_path/MovieCharactersACL2016/Database/annotation_imdb.annotation

Alternatively, you can populate the database through the ESParser.java (line 74 in pom file)
   mvn exec:java -Dexec.inClass="gr.ics.moviecharactersacl2016.features.ESParser"


4. Generating Weka experimental setups
------------------------------------------------------------------------

In the DatabaseConncetion.java put the credentials and the connection
url for your database installation.

1. To import data to the sentiwordnet table:
 - change the main class in the pom file (uncomment line 79 and 80 and
   comment lines from 74 to 75 and 77 to 82)
 - then type in a terminal:
   mvn exec:java -Dexec.inClass="gr.ics.moviecharactersacl2016.utilities.SentiWordnetImporter"

2. To generate the master arff file will all the features (named data.arff):
 - change the main class in the pom file (uncomment line 74 and 75 and
   comment lines 76 to 82)
 - type in a terminal:
   mvn exec:java -Dexec.inClass="gr.ics.moviecharactersacl2016.features.ArffCreation"

3. To create the rest of the arff files, each only including some of the features:
 - change the main class in the pom file (uncomment line 77 and 78 and
   comment lines 79 to 82)
 - type in a terminal
   mvn exec:java -Dexec.inClass="gr.ics.moviecharactersacl2016.features.ArffCreationSelection"

The folder ExperimentalSetup contains the files created in this step.

 - data.arff (all features)
 - data_only_domain_features.arff
 - data_only_phonological_features.arff
 - data_without_domain_features.arff
 - data_without_emotion_features.arff
 - data_without_poetic_features.arff
 - data_without_social_features.arff


5. Reproducing the ACL 2016 Experiments
------------------------------------------------------------------------

You can easily reproduce the results presented in the paper by loading
the appropriate arff file and selecting the algorithms Naive Bayes and
J48 with the predefined options in the Weka GUI.

Prior to this you must apply the filter Resample
(supervised-->instance-->Resample) with the predefined options due to
the imbalance of the data. Alternatively, you can achieve the same
effect by executing the main method in the class
ics.moviecharactersacl2016.features.WekaExperiments as follows:

 - change the main class in the pom file (uncomment line 81 and 82 and
   comment lines 74 to 80)

 - type in a terminal
   mvn exec:java -Dexec.inClass="gr.ics.moviecharactersacl2016.features.WekaExperiments"


                                Current Results
all features                  0.819  0.816  0.817
only domain features          0.725  0.709  0.717
only phonological features    0.774  0.766  0.768
without phonological features 0.772  0.765  0.767
without consonance features   0.828  0.824  0.825
without domain features       0.798  0.795  0.796
without emotion features      0.820  0.816  0.817
without poetic features       0.804  0.798  0.799
without social features       0.817  0.813  0.815

                                ACL 2016 Results 
all features                  0.824  0.822  0.823
only domain features          0.725  0.699  0.667
only phonological features    0.790  0.786  0.787
without phonological features 0.798  0.792  0.793
without consonance features   0.823  0.820  0.821
without domain features       0.803  0.801  0.802
without emotion features      0.814  0.810  0.811
without poetic features       0.836  0.832  0.833
without social features       0.807  0.803  0.804

The differences are due to the following:

a. the different version of eSpeak. More specifically, for the
ACL 2016 experiments we used version 1.46.02, while the current
version is 1.48.15. Between these versions, corrections have
been made in the rules, list, and dict files for the English
language [5], which impacts the phonological features.

b. a minor bug in the script that retrieves the crediting order of
the cast has been noticed and fixed, marginally improving the domain
features.

Overall the differences are small and do not affect the conclusions
drawn from these experiments in the paper. The poetic/stylistic
features are now shown to have more value, whereas in the 2016
experiments only consonance had some value. Since these features
operate upon the phonological transcription, it is possible that
the changes in the eSpeak models have increased their discriminative
power.



6. References
------------------------------------------------------------------------

[1] http://sentiwordnet.isti.cnr.it/SentiWordNet_3.0.0.tgz
[2] https://github.com/hypothesis/h
[3] http://espeak.sourceforge.net
[4] Papantoniou and Konstantopoulos,
    "Unravelling Names of Fictional Characters"
    In: Proceedings of ACL 2016, Berlin, 7 - 12 August 2016
    https://aclweb.org/anthology/P/P16/P16-1203.pdf
[5] https://github.com/nvaccess/espeak/blob/master/ChangeLog.txt#L74
